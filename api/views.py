from django.views.generic import View
import json
import urllib.request
from django.http import HttpResponse

class getSchoolApi(View):

	def get(self, request, *args, **kwargs):
		response = json.loads(urllib.request.urlopen("https://data.cityofnewyork.us/resource/734v-jeq5.json").read().decode())
		output = []
		for school in response:
			if school["num_of_sat_test_takers"].isdigit():
				obj = {}
				obj["test_takers"] = int(school["num_of_sat_test_takers"])
				obj["name"] = school["school_name"]
				obj["avg_critical"] = school["sat_critical_reading_avg_score"]
				obj["avg_math"] = school["sat_math_avg_score"]
				obj["avg_writing"] = school["sat_writing_avg_score"]
				output.append(obj)

		output = sorted(output, key=lambda k: k['test_takers'], reverse=True)

		return HttpResponse(json.dumps(output))