from django.http import HttpResponseRedirect
from django.views.generic import View
from django.template.response import TemplateResponse

# Create your views here.
class MainView(View):
    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, 'base.html')